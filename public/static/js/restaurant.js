const LIST = document.getElementById("restaurants-list")
const RIGHTDIV = document.getElementById("right-div")
const IMAGEDIV = document.getElementById("image-div")
let restaurants

/**
 * Fetches the data from 'data.json' and calls a function
 * to handle the data
 */
function getData() {
    fetch("/data", {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(data => 
        {
            // set to a global variable
            restaurants = data
            // call function to handle data
            addToHtml(data)
        }
    )
}

/**
 * Creates a list and adds the restaurant names to the list.
 * @param {data from data.json} data 
 */
function addToHtml(data) {
    let wrapper, listItem            
        
    for(let i=0; i<data.restaurants.length;i++) {
        listItem = document.createElement("li")
        listItem.innerHTML = data.restaurants[i].name
        wrapper = document.createElement("a");
        wrapper.href = 'javascript:displayInfo('+ data.restaurants[i].id +')'
        wrapper.appendChild(listItem)
        LIST.appendChild(wrapper)
    } 
}

/**
 * Displays the information of the restaurant that has been  pressed.
 * @param {id of restaurant} id 
 */
function displayInfo(id) {
    RIGHTDIV.innerHTML= ""
    IMAGEDIV.innerHTML = ""
    let heading = document.createElement("h2")
    heading.innerHTML = restaurants.restaurants[id].name
    let image = document.createElement("img")
    image.src = restaurants.restaurants[id].image
    image.width = 400;
    let location = document.createElement("p")
    location.innerHTML = restaurants.restaurants[id].location
    let description = document.createElement("p")
    description.innerHTML = restaurants.restaurants[id].description
    let rating = document.createElement("p")
    rating.innerHTML = "Average rating: " + restaurants.restaurants[id].rating

    RIGHTDIV.appendChild(heading)
    RIGHTDIV.appendChild(location)
    RIGHTDIV.appendChild(description)
    RIGHTDIV.appendChild(rating)
    IMAGEDIV.appendChild(image)

    let review
    for(let i=0; i<restaurants.restaurants[id].reviews.length; i++) {
        review = document.createElement("p")
        review.innerHTML = restaurants.restaurants[id].reviews[i].author + "'s review: \"" 
        + restaurants.restaurants[id].reviews[i].review + "\""
        RIGHTDIV.appendChild(review)
    }

   
}